package de.sem.project;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Employee, Long> {
		
	Optional<Employee> findBypersonFirstname(String name);
	
	Optional<Employee> findBypersonLastname(String lastname);

}
