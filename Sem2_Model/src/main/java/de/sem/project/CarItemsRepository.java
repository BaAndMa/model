package de.sem.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CarItemsRepository extends JpaRepository<CartItems, Long> {
	Optional<CartItems> findBycartItemsID(Long id);
}
