package de.sem.project;

import java.sql.Blob;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.sun.jmx.snmp.Timestamp;

@Entity
@Table(name="Product")

public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="productID")
	private Long productID;

	@Column(name="productInitDate")
	private Timestamp productInitDate;
	@Column(name="productName")
	private String productName;
	@Column(name="productDescription")
	private String productDescription;
	@Column(name="productPrice")
	private Long productPrice;
	@Column(name="productCatchphrase")
	private String productCatchphrase;

	@Lob
	@Column(name="productImage")
	private Blob productImage;

	
	@ManyToMany(mappedBy="products")
    private Set<ProductCategory> productCategorys = new HashSet<ProductCategory>();
	
	
	
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="Bundle_Product", 
                joinColumns={@JoinColumn(name="productID")}, 
                inverseJoinColumns={@JoinColumn(name="BundleID")})
    private Set<Bundle> productBundles = new HashSet<Bundle>();
	
	public Timestamp getProductInitDate() {
		return productInitDate;
	}

	public void setProductInitDate(Timestamp productInitDate) {
		this.productInitDate = productInitDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Long getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Long productPrice) {
		this.productPrice = productPrice;
	}

	public Blob getProductImage() {
		return productImage;
	}

	public void setProductImage(Blob productImage) {
		this.productImage = productImage;
	}

	public String getProductCatchphrase() {
		return productCatchphrase;
	}

	public void setProductCatchphrase(String productCatchphrase) {
		this.productCatchphrase = productCatchphrase;
	}

	public Long getProductID() {
		return productID;
	}

}
