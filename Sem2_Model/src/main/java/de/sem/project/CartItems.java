package de.sem.project;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "CartItems")
@Entity
public class CartItems {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cartItemsID")
	private Long cartItemsID;

	@ManyToOne
	@JoinColumn(name = "shoppingcartID")
	@Column(name = "shoppingcartID")
	private Shoppingcart shoppingcart;

	@Column(name = "CartItemStartDate")
	private Timestamp startdate;

	@Column(name = "CartItemEndDate")
	private Timestamp enddate;

	@ManyToMany(mappedBy = "cartItems")
	private Set<Device> devices = new HashSet<Device>();

	public Shoppingcart getShoppingcart() {
		return shoppingcart;
	}

	public void setShoppingcart(Shoppingcart shoppingcart) {
		this.shoppingcart = shoppingcart;
	}

	public Timestamp getStartdate() {
		return startdate;
	}

	public void setStartdate(Timestamp startdate) {
		this.startdate = startdate;
	}

	public Timestamp getEnddate() {
		return enddate;
	}

	public void setEnddate(Timestamp enddate) {
		this.enddate = enddate;
	}

	public Long getCartItemsID() {
		return cartItemsID;
	}

}
