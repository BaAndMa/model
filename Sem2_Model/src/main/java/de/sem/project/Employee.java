package de.sem.project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Employee")
public class Employee extends Person{

	@Column(name="employeePhone")
	private String employeePhone;
}
