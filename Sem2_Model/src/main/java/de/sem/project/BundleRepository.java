package de.sem.project;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BundleRepository extends JpaRepository<Bundle, Long> {
    Optional<Bundle> findBybundleID(Long id);
}