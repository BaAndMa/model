package de.sem.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	Optional<Employee> findBypersonID(Long id);
	
	Optional<Employee> findBypersonFirstname(String name);
	
	Optional<Employee> findBypersonLastname(String lastname);

}
