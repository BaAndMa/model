package de.sem.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<Device, Long> {
	Optional<Device> findBydeviceID(Long id);
}
