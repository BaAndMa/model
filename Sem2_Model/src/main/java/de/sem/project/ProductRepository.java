package de.sem.project;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
	Optional<Product> findByproductID(Long id);
	
	Optional<Product> findByproductName(String name);
	

}
