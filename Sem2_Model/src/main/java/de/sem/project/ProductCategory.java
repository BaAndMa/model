package de.sem.project;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ProductCategory")

public class ProductCategory {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="categoryID")
	private Long categoryID;
	
	@Column(name="categoryName")
	private String categoryName;

	@ManyToOne
	@Column(name="categoryParent")
	private ProductCategory categoryParent;
	
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="ProductCategory_Product", 
                joinColumns={@JoinColumn(name="categoryID")}, 
                inverseJoinColumns={@JoinColumn(name="productID")})
    private Set<Product> products = new HashSet<Product>();
	
	
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="ProductCategory_Bundle", 
                joinColumns={@JoinColumn(name="categoryID")}, 
                inverseJoinColumns={@JoinColumn(name="bundleID")})
    private Set<Bundle> bundles = new HashSet<Bundle>();
	
	
	public ProductCategory getCategoryParent() {
		return categoryParent;
	}


	public void setCategoryParent(ProductCategory categoryParent) {
		this.categoryParent = categoryParent;
	}


	public Long getCategoryID() {
		return categoryID;
	}


	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
