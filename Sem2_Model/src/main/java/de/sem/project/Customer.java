package de.sem.project;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Customer")
public class Customer extends Person{
	
	@Column(name="customerStree")
	private String customerStree;
	
	@Column(name="customerCity")
	private String customerCity;
	
	@Column(name="customerPostalCode")
	private Integer customerPostalCode;
	
	@Column(name="customerPhone")
	private String customerPhone;
	
	@Column(name="customerSecurityQuestion")
	private String customerSecurityQuestion;
	
	@Column(name="customerSecurityAnswer")
	private String customerSecurityAnswer;
	
	@Column(name="customerSince")
	private Timestamp customerSince;
}
