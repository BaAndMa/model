package de.sem.project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Person")
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="personID")
	private Long personID;
	
	@Column(name="personGender")
	private String personGender;
	
	@Column(name="personFirstname")
	private String personFirstname;
	
	@Column(name="personLastname")
	private String personLastname;
	
	@Column(name="personUsername")
	private String personUsername;
	
	@Column(name="personPassword")
	private String personPassword;
	
	
	

}
