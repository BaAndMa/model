package de.sem.project;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "Device")
@Entity
public class Device {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "deviceID")
	private Long deviceID;

	@ManyToOne
	@JoinColumn(name = "productID")
	private Product product;

	private DeviceState devideState;

	
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="CartItems_Product", 
                joinColumns={@JoinColumn(name="devideID")}, 
                inverseJoinColumns={@JoinColumn(name="cartItemsID")})
    private Set<CartItems> cartItems = new HashSet<CartItems>();
	
	@Enumerated(EnumType.STRING)
	public DeviceState getDevideState() {
		return devideState;
	}

	@Enumerated(EnumType.STRING)
	public void setDevideState(DeviceState devideState) {
		this.devideState = devideState;
	}

	public Long getDeviceID() {
		return deviceID;
	}

}
