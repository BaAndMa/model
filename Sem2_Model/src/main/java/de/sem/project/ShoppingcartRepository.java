package de.sem.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoppingcartRepository extends JpaRepository<Shoppingcart, Long> {
	Optional<Shoppingcart> findByperson(Long id);
	
	Optional<Shoppingcart> findBycartBooked(boolean booked);
	

}