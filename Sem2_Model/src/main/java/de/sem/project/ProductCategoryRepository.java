package de.sem.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
	Optional<ProductCategory> findBycategoryID(Long id);
	
	Optional<ProductCategory> findBycategoryName(String name);
	

}
