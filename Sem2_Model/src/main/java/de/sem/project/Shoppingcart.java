package de.sem.project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Shoppingcart")
@Entity
public class Shoppingcart {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "shoppingcartID")
	private Long shoppingcartID;

	@Column(name = "CartBooked")
	private boolean CartBooked;

	@ManyToOne
	@JoinColumn(name = "shoppingcartID")
	@Column(name = "customerID")
	private Person person;

	public boolean isCartBooked() {
		return CartBooked;
	}

	public void setCartBooked(boolean cartBooked) {
		CartBooked = cartBooked;
	}

	public Long getShoppingcartID() {
		return shoppingcartID;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
