package de.sem.project;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Bundle")
public class Bundle {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="bundleID")

    private Long bundleID;
    
	@Column(name="bundleDiscount")
    private Double bundleDiscount;

	@Column(name="bundleDiscription")
	private String bundleDiscription;

	
	@ManyToMany(mappedBy="bundles")
    private Set<ProductCategory> productCategorys = new HashSet<ProductCategory>();
	
	
	@ManyToMany(mappedBy="productBundles")
    private Set<Bundle> productBundles = new HashSet<Bundle>();
	
	public Double getBundleDiscount() {
		return bundleDiscount;
	}

	public void setBundleDiscount(Double bundleDiscount) {
		this.bundleDiscount = bundleDiscount;
	}

	public String getBundleDiscription() {
		return bundleDiscription;
	}

	public void setBundleDiscription(String bundleDiscription) {
		this.bundleDiscription = bundleDiscription;
	}

	public Long getBundleID() {
		return bundleID;
	}
}
